﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fraction
{
    class Fraction
    {
        private long numerator;
        private long denominator;
        public Fraction()
        {
            numerator = 0;
            denominator = 1;

        }
        public Fraction(long numerator, long denominator)
        {

            Numerator = numerator;
            Denominator = denominator;
            Reduce();

        }
        public long Numerator
        {
            get 
            { 
                return numerator;
            }
            set
            { 
                numerator = value;
            }
        }
        public long  Denominator
        {
            get
            {
                return denominator;
            }
            set
            {
                if (value==0)
                    throw new DivideByZeroException("Деление на ноль!");
                 denominator = value;
            }
        }
        public static Fraction operator -(Fraction op1)
        {
            return new Fraction(-op1.Numerator, op1.Denominator);
        }
        public static Fraction operator +(Fraction op1, Fraction op2)
        {
            //Fraction result = new Fraction();
            //result.х = op1.x + ор2.х; 
            long common = Lcm(op1.Denominator,op2.Denominator);
            long n1 = common / op1.Denominator;
            long n2 = common / op2.Denominator;
            long res = checked(op1.Numerator * n1 + op2.Numerator * n2);
            return new Fraction(op1.Numerator * n1 + op2.Numerator * n2, common);
           // return result;
        }
        public static Fraction operator -(Fraction op1, Fraction op2)
        {

            long common = Lcm(op1.Denominator, op2.Denominator);
            long n1 = common / op1.Denominator;
            long n2 = common / op2.Denominator;
            return new Fraction(op1.Numerator * n1 - op2.Numerator * n2, common);
        }
        public static Fraction operator *(Fraction op1, Fraction op2)
        {
            long res1 = checked(op1.Numerator * op2.Numerator);
            long res2 = checked(op1.Denominator * op2.Denominator);
            return new Fraction(op1.Numerator * op2.Numerator, op1.Denominator* op2.Denominator);
        }
        public static Fraction operator /(Fraction op1, Fraction op2)
        {
           
            return new Fraction(op1.Numerator * op2.Denominator, op1.Denominator * op2.Numerator);
        }
        public static bool operator >=(Fraction op1, Fraction op2)
        {
            return (op1.Numerator * op2.Denominator - op1.Denominator * op2.Numerator >= 0);
        }
        public static bool operator <=(Fraction op1, Fraction op2)
        {
            return (op1.Numerator * op2.Denominator - op1.Denominator * op2.Numerator <= 0);
        }
        public static bool operator >(Fraction op1, Fraction op2)
        {
            return !(op1 <=op2);
        }
        public static bool operator <(Fraction op1, Fraction op2)
        {
            return !(op1>= op2);
        }
        public static bool operator ==(Fraction op1, Fraction op2)
        {
            return (op1.Numerator==op2.Numerator && op1.Denominator==op2.Denominator);
        }
        public static bool operator !=(Fraction op1, Fraction op2)
        {
            return !(op1 == op2);
        }
        public override bool Equals(Object obj)
        {
            if ((obj == null) || !(obj is Fraction))
            {
                return false;
            }

            else
            {
                Fraction f = (Fraction)obj;
                return (Numerator == f.Numerator) && (Denominator == f.Denominator);
            }
        }
        public override int GetHashCode()
        {
            return Tuple.Create(Numerator, Denominator).GetHashCode();

        }
       
        
        private void Reduce()
        {  
            long _gcd = Gcd(Numerator, Denominator);
            Numerator /= _gcd;
            Denominator /= _gcd;

            if (Denominator < 0)
            {
                Numerator *= -1;
                Denominator *= -1;
            }

        }
        public static  long Gcd(long a, long b)
        {
            if (b == 0)
                return a;
            else
                return Gcd(b, a % b);
        }
        public static long Lcm(long a, long b)
        {
            return (a / Gcd(a, b) * b);
        }
        public static implicit operator double(Fraction op1)
        {
            return (double)op1.Numerator / op1.Denominator;
        }
        public override string ToString()
        {

            if (Numerator == 0)
                return ($"{Numerator}");
            else
             return ($"{Numerator} / {Denominator}");
        }
       
    }
    
}
