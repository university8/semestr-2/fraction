﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Fraction;

namespace Fraction
{
	class Program
	{
		static void Main(string[] args)
		{
			Fraction f1 = new Fraction();
			Fraction f2 = new Fraction();
			Console.WriteLine("Введите первую дробь: ");
			while (true)
			{
				try
				{
					f1.Numerator = long.Parse(Console.ReadLine());
					f1.Denominator = long.Parse(Console.ReadLine());
					break;
				}
				catch (Exception ex)
				{
					Console.WriteLine("Ошибка ввода!\n" + ex.Message);
					continue;
				}

			}
			Console.WriteLine("Введите вторую дробь: ");
			while (true)
			{
				try
				{
					f2.Numerator = long.Parse(Console.ReadLine());
					f2.Denominator = long.Parse(Console.ReadLine());
					break;
				}
				catch (Exception ex)
				{
					Console.WriteLine("Ошибка ввода!\n" + ex.Message);
					continue;
				}
			}

			Console.WriteLine("Что вы хотите найти? ");
			Console.WriteLine("1.Сумма");
			Console.WriteLine("2.Разность");
			Console.WriteLine("3.Произведение");
			Console.WriteLine("4.Отношение");
			Console.WriteLine("5.Унарный минус");
			Console.WriteLine("6.Операции сравнения: ");
			short op= short.Parse(Console.ReadLine());
			try
			{
				switch (op)
				{
					case 1:
						Console.WriteLine($"Результат: { f1 + f2}");
						break;
					case 2:
						Console.WriteLine($"Результат: { f1 - f2}");
						break;
					case 3:
						Console.WriteLine($"Результат: { f1 * f2}");
						break;
					case 4:
						Console.WriteLine($"Результат: { f1 / f2}");
						break;
					case 5:
						Console.WriteLine("Результат: " + (-f1).ToString());
						break;
					case 6:
						Console.WriteLine($"A==B: {f1 == f2}");
						Console.WriteLine($"A!=B: {f1 != f2}");
						Console.WriteLine($"A>B: {f1 > f2}");
						Console.WriteLine($"A<B: {f1 < f2}");
						Console.WriteLine($"A>=B: {f1 >= f2}");
						Console.WriteLine($"A<=B: {f1 <= f2}");
						break;
					default:
						Console.WriteLine("Выберите операцию");
						break;
				}
			}
			catch (OverflowException e)
			{
				Console.WriteLine("Ошибка!\n" + e.Message);
			}

		}
	}
}
